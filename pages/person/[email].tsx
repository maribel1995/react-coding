import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { Button, PageHeader, Descriptions, Input, message } from "antd";

import { withContextInitialized } from "../../components/hoc";
import CompanyCard from "../../components/molecules/CompanyCard";
import GenericList from "../../components/organisms/GenericList";
import OverlaySpinner from "../../components/molecules/OverlaySpinner";
import { usePersonInformation } from "../../components/hooks/usePersonInformation";

import { Company } from "../../constants/types";
import { ResponsiveListCard } from "../../constants";

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [isEditing, setIsEditing] = useState(false);
  const [newData, setNewData] = useState(data);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return (
      <OverlaySpinner title={`Loading ${router.query?.email} information`} />
    );
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push("/home")
    );
    return <></>;
  }

  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewData((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleClickEdit = () => {
    if (isEditing) {
      save(newData);
    }
    setIsEditing((prevState) => !prevState);
  };

  const renderComponent = ({ value, name }) =>
    isEditing ? (
      <Input value={value} name={name} onChange={handleChange} />
    ) : (
      value
    );

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleClickEdit}>
            {isEditing ? "Save" : "Edit"}
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">
              {renderComponent({ value: newData.name, name: "name" })}
            </Descriptions.Item>
            <Descriptions.Item label="Gender">
              {renderComponent({ value: newData.gender, name: "gender" })}
            </Descriptions.Item>
            <Descriptions.Item label="Phone">
              {renderComponent({ value: newData.phone, name: "phone" })}
            </Descriptions.Item>
            <Descriptions.Item label="Birthday">
              {renderComponent({ value: newData.birthday, name: "birthday" })}
            </Descriptions.Item>
          </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
